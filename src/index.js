import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./component/Module";
import Styled from "./Styled";
import "./index.css";
import Layout from "./component/Layout";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Layout />

    {/* <main>
      <h1>Hello World</h1>
    </main>

    <Module />

    <Styled /> */}
  </React.StrictMode>
);
