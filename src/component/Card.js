import { Card, Col, Row } from 'antd';
import "./Card.css";
import Modal from "./Modal";

const imagetStyle = {
  width: '100%',
}

export default () => (
  <div className="site-card-wrapper">
    <Row gutter={16}>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://akcdn.detik.net.id/visual/2019/02/28/a02dae3a-28ca-49ce-8596-2f165188872f_169.jpeg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://akcdn.detik.net.id/visual/2019/02/28/a02dae3a-28ca-49ce-8596-2f165188872f_169.jpeg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Mobil" bordered={false}>
          <img
            style={imagetStyle}
            src="https://akcdn.detik.net.id/visual/2019/02/28/a02dae3a-28ca-49ce-8596-2f165188872f_169.jpeg"
            alt=""
          />
          <Modal />
        </Card>
      </Col>
    </Row>
  </div>
);
